package com.anji.sp.push.service;

import com.anji.sp.model.RequestModel;
import com.anji.sp.model.ResponseModel;
import com.anji.sp.push.model.po.PushConfiguresPO;
import com.anji.sp.push.model.vo.PushConfiguresVO;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 推送配置项 服务类
 * </p>
 *
 * @author kean
 * @since 2021-01-20
 */
public interface PushConfiguresService extends IService<PushConfiguresPO> {

    /**
     * 创建
     *
     * @param pushConfiguresVO
     * @return
     */
    ResponseModel create(PushConfiguresVO pushConfiguresVO);

    /**
     * 根据id修改
     *
     * @param pushConfiguresVO
     * @return
     */
    ResponseModel updateById(PushConfiguresVO pushConfiguresVO);

    /**
     * 根据id删除
     *
     * @param pushConfiguresVO
     * @return
     */
    ResponseModel deleteById(PushConfiguresVO pushConfiguresVO);

    /**
     * 根据id查询一条记录
     *
     * @param pushConfiguresVO
     * @return
     */
    ResponseModel queryById(PushConfiguresVO pushConfiguresVO);

    /**
     * 根据appKey或appId查询一条记录
     *
     * @param pushConfiguresVO
     * @return
     */
    ResponseModel select(PushConfiguresVO pushConfiguresVO);

    /**
     * 根据appkey 或 secretKey查询
     * @param pushConfiguresVO
     * @return
     */
    ResponseModel selectOne(PushConfiguresVO pushConfiguresVO);

    /**
     * 根据appKey查询一条记录
     *
     * @param pushConfiguresVO
     * @return
     */
    ResponseModel queryByAppKey(PushConfiguresVO pushConfiguresVO);

    /**
     * 根据参数分页查询列表
     *
     * @param pushConfiguresVO
     * @return
     */
    ResponseModel queryByPage(PushConfiguresVO pushConfiguresVO);

}
