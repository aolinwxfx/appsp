package com.anji.sp.push.common;

/**
 * 设备类型
 */
public class DeviceType {
    public static final String HW = "1";//华为
    public static final String XM = "2";//小米
    public static final String OP = "3";//Oppo
    public static final String VO = "4";//Vivo
    public static final String IOS = "5";//ios
    public static final String OTHER = "0";//其他手机
}
