module.exports = {
    title: '加加移动服务平台',
    description: 'Just playing around',
    base: '/appsp-doc/',
    themeConfig: {
        nav: [
            // {
            //     text: '演示',
            //     link: 'https://openappsp.anji-plus.com'
            // },
            {
                text: 'gitee',
                link: 'https://gitee.com/anji-plus/appsp'
            },
            {
                text: 'github',
                link: '/'
            },
            // {
            //     text: 'git',
            //     items: [
            //         {
            //             text: 'ES6',
            //             link: 'https://gitee.com/anji-plus/appsp'
            //         },
            //         {
            //             text: 'Vue',
            //             link: '/js/vue/'
            //         }
            //     ]
            // }
        ],
      //   sidebar: {
      //     '/server/': [
      //         {
      //             title: 'Group1',  //组名
      //             children: [
      //                 ['', '测试']
      //             ]
      //         },
      //         {
      //             title: 'Group2',
      //             children: [
      //                 ['server', '测试2']
      //             ],
      //         }
      //     ],
      //     '/web/': [
      //       {
      //           title: 'Group3',  //组名
      //           children: [
      //               ['', '测试3']
      //           ]
      //       },
      //       {
      //           title: 'Group4',
      //           children: [
      //               ['web', '测试4']
      //           ],
      //       }
      //   ]
      // }
        sidebar: [{
          collapsable: false,
            sidebarDepth: 3,
          children: [
            '/server/snapshot',
          ]
        },{
          title: '版本&公告',
          collapsable: false,
          sidebarDepth: 3,
          children: [
	    '/operation/operation-tips',
            '/server/deployment',
            '/server/ProjectIntroduction',
            '/server/server',
            '/web/web',
            '/app/ios/ios', 
            '/app/android/android', 
            '/app/flutter/flutter', 
            '/updateLog'
          ]
        },
        {
          title: '推送',
          collapsable: false,
          sidebarDepth: 3,
          children: [
            '/push/push_introduce',
			 '/push/push-test',
            '/push/service',
            '/push/hw',
            '/push/xm',
            '/push/vivo',
            '/push/oppo',
            '/push/jpush',
            '/push/IntegratedDoc',
			'/push/download'
          ]
        },
        {
          title: '其他',
          collapsable: false,
          children: [
            '/other/question',
          ]
        }]
    }
}
