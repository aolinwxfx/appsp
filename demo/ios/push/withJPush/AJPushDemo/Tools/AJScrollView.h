//
//  AJScrollView.h
//  AJPushDemo
//
//  Created by 李家斌 on 2021/3/19.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AJScrollView : UIScrollView

@end

NS_ASSUME_NONNULL_END
