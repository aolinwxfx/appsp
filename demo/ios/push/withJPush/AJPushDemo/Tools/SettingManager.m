//
//  SettingManager.m
//  AJPushDemo
//
//  Created by Black on 2021/2/4.
//

#import "SettingManager.h"

#define Save_JPushKey @"Save_JPushKey"
#define Save_AppSpKey @"Save_AppSpKey"
#define Save_AppSpUrl @"Save_AppSpUrl"
#define Save_AppAppSecretKey @"Save_AppAppSecretKey"

@implementation SettingManager

+ (NSString *)getJPushKey {
    return [self getStringForKey:Save_JPushKey];
}
+ (NSString *)getAppSpKey {
    return [self getStringForKey:Save_AppSpKey];
}
+ (NSString *)getAppSpUrl {
    return [self getStringForKey:Save_AppSpUrl];
}
+ (NSString *)getAppSpSecretKey{
    return [self getStringForKey:Save_AppAppSecretKey];
}

+ (void)setJPushKey:(NSString *)jpushKey {
    [self saveString:jpushKey forKey:Save_JPushKey];
}

+ (void)setAppSpKey:(NSString *)appSpKey {
    [self saveString:appSpKey forKey:Save_AppSpKey];
}

+ (void)setAppSpUrl:(NSString *)appSpUrl {
    [self saveString:appSpUrl forKey:Save_AppSpUrl];
}

+ (void)setAppSpSecretKey:(NSString *)secretKey{
    [self saveString:secretKey forKey:Save_AppAppSecretKey];
}

+ (NSString *)getStringForKey:(NSString *)key {
    NSUserDefaults *userDef = [NSUserDefaults standardUserDefaults];
    return [userDef valueForKey:key];
}
+ (void)saveString:(NSString *)value forKey:(NSString *)key {
    NSUserDefaults *userDef = [NSUserDefaults standardUserDefaults];
    if (value != nil && key != nil) {
        [userDef setValue:value forKey:key];
        [userDef synchronize];
    }
}

@end
