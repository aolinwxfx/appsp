//
//  SettingController.m
//  AJPushDemo
//
//  Created by Black on 2021/2/4.
//  Copyright © 2018 anji-plus 安吉加加信息技术有限公司 http://www.anji-plus.com. All rights reserved.
//

#import "SettingController.h"
#import "SettingManager.h"

@interface SettingController ()
@property (weak, nonatomic) IBOutlet UITextField *jpushText;
@property (weak, nonatomic) IBOutlet UITextField *appspKeyText;
@property (weak, nonatomic) IBOutlet UITextField *appspUrlTxt;
@property (weak, nonatomic) IBOutlet UITextField *secretKeyText;

@end

@implementation SettingController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"配置中心";
    self.jpushText.text = [SettingManager getJPushKey];
    self.appspKeyText.text = [SettingManager getAppSpKey];
    self.appspUrlTxt.text = [SettingManager getAppSpUrl];
    self.secretKeyText.text = [SettingManager getAppSpSecretKey];

    
}
- (IBAction)saveBtnClick:(id)sender {
    [self.view endEditing:YES];
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"是否确认修改" message:nil preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *conform = [UIAlertAction actionWithTitle:@"保存" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        //处理跳转
        [self saveConfigure];
    }];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    }];
    [alert addAction:conform];
    [alert addAction:cancel];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)saveConfigure {
    [SettingManager setJPushKey:self.jpushText.text];
    [SettingManager setAppSpKey:self.appspKeyText.text];
    [SettingManager setAppSpUrl:self.appspUrlTxt.text];
    [SettingManager setAppSpSecretKey:self.secretKeyText.text];
    NSLog(@"jpushKey: %@", self.jpushText.text);
    NSLog(@"appspKey: %@", self.appspKeyText.text);
    NSLog(@"appspUrl: %@", self.appspUrlTxt.text);
    NSLog(@"secretKey: %@", self.secretKeyText.text);
    dispatch_queue_t queue = dispatch_get_main_queue();
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), queue, ^{
        [self.navigationController popViewControllerAnimated:YES];
    });
}

//屏幕手势事件
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}


- (void)dealloc {
    NSLog(@"SettingController dealloc");
}

@end
