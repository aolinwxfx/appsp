//
//  AJPushSDK.h
//  AJPushSDK
//
//  Created by Black on 2021/2/2.
//  Copyright © 2018 anji-plus 安吉加加信息技术有限公司 http://www.anji-plus.com. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for AJPushSDK.
FOUNDATION_EXPORT double AJPushSDKVersionNumber;

//! Project version string for AJPushSDK.
FOUNDATION_EXPORT const unsigned char AJPushSDKVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <AJPushSDK/PublicHeader.h>
#import <AJPushSDK/AJPushService.h>

