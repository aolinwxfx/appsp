import 'dart:io';

class NotificationMessageModel {
  String title;
  String content;
  int actionType; //1，无操作，2，点击通知栏消息
  int brandType; //设备类型，1，华为，2，小米，3，oppo，4，vivo，5，其他
  dynamic notificationExtras; //自定义参数
  int messageType; ////消息类型  1、极光透传，2、极光通知栏声音，3小米极光点击通知栏跳转页面

  NotificationMessageModel(
      {this.title,
      this.content,
      this.actionType,
      this.brandType,
      this.notificationExtras,
      this.messageType});

  NotificationMessageModel.fromJson(Map<String, dynamic> json) {
    title = json['title'];
    content = json['content'];
    actionType = json['actionType'];
    brandType = json['brandType'];
    if (Platform.isIOS) {
      notificationExtras = json['extras'] ?? null;
    } else {
      notificationExtras = json['notificationExtras'] ?? null;
    }
    messageType = json['messageType'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['title'] = this.title;
    data['content'] = this.content;
    data['actionType'] = this.actionType;
    data['brandType'] = this.brandType;
    data['notificationExtras'] = this.notificationExtras;
    data['messageType'] = this.messageType;
    return data;
  }

  @override
  String toString() {
    return 'NotificationMessageModel{title: $title, content: $content, actionType: $actionType, brandType: $brandType, notificationExtras: $notificationExtras, messageType: $messageType}';
  }
}
