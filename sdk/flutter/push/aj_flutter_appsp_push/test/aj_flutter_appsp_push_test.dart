import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:aj_flutter_appsp_push/aj_flutter_appsp_push.dart';

void main() {
  const MethodChannel channel = MethodChannel('aj_flutter_appsp_push');

  TestWidgetsFlutterBinding.ensureInitialized();

  setUp(() {
    channel.setMockMethodCallHandler((MethodCall methodCall) async {
      return '42';
    });
  });

  tearDown(() {
    channel.setMockMethodCallHandler(null);
  });

  test('getPlatformVersion', () async {
    expect(await AjFlutterAppspPush.platformVersion, '42');
  });
}
