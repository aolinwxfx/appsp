package io.dcloud.uniplugin;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.util.Log;

import androidx.core.content.FileProvider;

import com.alibaba.fastjson.JSONObject;
import com.anji.appsp.sdk.AppSpConfig;
import com.anji.appsp.sdk.AppSpLog;
import com.anji.appsp.sdk.model.AppSpModel;
import com.anji.appsp.sdk.model.AppSpVersion;
import com.anji.appsp.sdk.version.service.IAppSpVersionCallback;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import io.dcloud.feature.uniapp.annotation.UniJSMethod;
import io.dcloud.feature.uniapp.bridge.UniJSCallback;
import io.dcloud.feature.uniapp.common.UniModule;

/**
 * Copyright © 2018 anji-plus
 * 安吉加加信息技术有限公司
 * http://www.anji-plus.com
 * All rights reserved.
 *
 * 版本更新，uni vue/nvue和插件通信
 */
public class AppspModule extends UniModule {

    String TAG = "AppspModule";
    public static int REQUEST_CODE = 1000;

    /**
     * 在请求前，记得先获取存储和版本升级权限
     *
     * @param options
     */
    @UniJSMethod(uiThread = false)
    public void initAppsp(JSONObject options) {
        String appKey = options.getString("appKey");
        String host = options.getString("host");
        AppSpLog.d(" appkey is " + appKey);
        AppSpLog.d(" host is " + host);
        AppSpConfig.getInstance().init(mUniSDKInstance.getContext(), appKey).setHost(host);
    }

    @UniJSMethod(uiThread = false)
    public void getVersionModel(final UniJSCallback callback) {
        final JSONObject data = new JSONObject();
        AppSpConfig.getInstance().getVersion(new IAppSpVersionCallback() {
            @Override
            public void update(AppSpModel<AppSpVersion> spModel) {
                AppSpVersion version = spModel.getRepData();
                if (version != null) {
                    data.put("code", "success");
                    //下载Url
                    data.put("downloadUrl", version.getDownloadUrl());
                    //更新日志
                    data.put("updateLog", version.getUpdateLog());
                    //是否要显示更新
                    data.put("showUpdate", version.isShowUpdate());
                    //是否强制更新
                    data.put("mustUpdate", version.isMustUpdate());
                    data.put("msg", "Get Appsp Info success");
                    callback.invoke(data);
                } else {
                    data.put("code", "latest");
                    data.put("msg", "Empty data");
                    callback.invoke(data);
                }
            }

            @Override
            public void error(String code, String msg) {
                if ("0000".equals(code)) {
                    data.put("code", "latest");
                    data.put("msg", "Empty data");
                } else {
                    data.put("code", "fail");
                    data.put("msg", "Parse error");
                }
                callback.invoke(data);
            }
        });
    }

    /**
     * apk下载
     *
     * @param options
     */
    @UniJSMethod(uiThread = false)
    public void downloadApk(JSONObject options) {
        String downloadUrl = options.getString("downloadUrl");
        File file = null;
        boolean isInteriorSdCardPrivate = false;
        String fileName = mUniSDKInstance.getContext().getPackageName() + "_" + System.currentTimeMillis() + ".apk";
        if (Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED)) {
            file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), fileName);
        } else {
            isInteriorSdCardPrivate = true;
            file = new File(mUniSDKInstance.getContext().getFilesDir(), fileName);
        }
        if (file != null && file.exists()) {
            file.delete();
        }
        try {
            URL url = new URL(downloadUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            InputStream in = connection.getInputStream();
            int count = 0;
            int len;
            byte[] b = new byte[1024];
            FileOutputStream out;
            if (isInteriorSdCardPrivate) {
                out = mUniSDKInstance.getContext().openFileOutput(fileName, Context.MODE_WORLD_READABLE | Context.MODE_WORLD_WRITEABLE);
            } else {
                out = new FileOutputStream(file);
            }
            float rate = 0f;
            while ((len = in.read(b)) != -1) {
                out.write(b, 0, len);
                count += len;
                rate = 1.0f * count / connection.getContentLength();
                if (rate >= 1) {
                    installApk(file);
                    break;
                }
            }
            out.close();
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * apk安装
     *
     * @param options
     */
    @UniJSMethod(uiThread = false)
    public void installApk(JSONObject options) {
        String fileName = options.getString("fileName");
        File file = new File(fileName);
        Intent intent = new Intent(Intent.ACTION_VIEW);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            Uri contentUri = FileProvider.getUriForFile(mUniSDKInstance.getContext(), mUniSDKInstance.getContext().getPackageName() + ".fileprovider", file);
            intent.setDataAndType(contentUri, "application/vnd.android.package-archive");
        } else {
            intent.setDataAndType(Uri.fromFile(file), "application/vnd.android.package-archive");
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        }
        mUniSDKInstance.getContext().startActivity(intent);
    }

    public void installApk(File file) {
        Intent intent = new Intent(Intent.ACTION_VIEW);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            Uri contentUri = FileProvider.getUriForFile(mUniSDKInstance.getContext(), mUniSDKInstance.getContext().getPackageName() + ".fileprovider", file);
            intent.setDataAndType(contentUri, "application/vnd.android.package-archive");
        } else {
            intent.setDataAndType(Uri.fromFile(file), "application/vnd.android.package-archive");
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        }
        mUniSDKInstance.getContext().startActivity(intent);
    }

    //run ui thread
    @UniJSMethod(uiThread = true)
    public void testAsyncFunc(JSONObject options, UniJSCallback callback) {
        Log.e(TAG, "testAsyncFunc--" + options);
        if (callback != null) {
            JSONObject data = new JSONObject();
            data.put("code", "success");
            callback.invoke(data);
            //callback.invokeAndKeepAlive(data);
        }
    }

    //run JS thread
    @UniJSMethod(uiThread = false)
    public JSONObject testSyncFunc() {
        JSONObject data = new JSONObject();
        data.put("code", "success");
        return data;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE && data.hasExtra("respond")) {
            Log.e(TAG, "原生页面返回----" + data.getStringExtra("respond"));
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @UniJSMethod(uiThread = true)
    public void gotoNativePage() {
        if (mUniSDKInstance != null && mUniSDKInstance.getContext() instanceof Activity) {
            Intent intent = new Intent(mUniSDKInstance.getContext(), NativePageActivity.class);
            ((Activity) mUniSDKInstance.getContext()).startActivityForResult(intent, REQUEST_CODE);
        }
    }
}
